// Dummy Database
let posts = [];
let count = 1;

// Create/Add Posts
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	// this prevents the webpage from reloading/refreshing
	e.preventDefault();

	// acts like a schema/model of the document
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector('#txt-body').value
	});

	// this is responsible to increment the ID value
	count++;

	console.log(posts);
	showPost(posts);
	alert('Successfully Added');
})

// Display/Show Posts
// created a function to show all the data in our database
const showPost = (posts) => {

	// contain all individual posts
	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id='post-${post.id}'>
				<h3 id='post-title-${post.id}'>${post.title}</h3>
				<p id='post-body-${post.id}'>${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;

	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Posts
const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector('#txt-edit-id').value = postId;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

// Update Posts
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	// TO FIND THE SPECIFIC POST WE WANT TO EDIT
	for(let i=0; i<posts.length; i++) {

		// The values post[i].id is a NUMBER while document.querySelector("#txt-edit-id").value is a STRING
		// Therefore, it is necessary to convert the NUMBER to a STRING
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPost(posts);
			alert("Successfully Updated!");

			break;
		}
	}
})

// Delete post
function deletePost(id) {
	posts.splice(posts.indexOf(id), 1);
	const element = document.getElementById(`post-${id}`);
	element.remove();
}
